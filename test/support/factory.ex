defmodule Retro.Factory do
  use ExMachina.Ecto, repo: Retro.Repo
  alias Retro.Retrospective.{Board, Column, Card, Vote}

  def board_factory do
    %Board{
      title: Faker.Pizza.meat()
    }
  end

  def column_factory do
    %Column{
      title: Faker.Pizza.topping(),
      rank: sequence(:column_rank, &(LexorankEx.middle(8) |> LexorankEx.next(&1))),
      board: build(:board)
    }
  end

  def card_factory do
    %Card{
      body: Faker.Lorem.paragraph(1..5),
      rank: sequence(:card_rank, &(LexorankEx.middle(8) |> LexorankEx.next(&1))),
      column: build(:column)
    }
  end

  def vote_factory do
    %Vote{
      card: build(:card)
    }
  end
end
