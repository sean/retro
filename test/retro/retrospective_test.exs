defmodule Retro.RetrospectiveTest do
  use Retro.DataCase
  import Retro.Factory
  alias Retro.Retrospective
  alias Retro.Retrospective.{Board, Column, Card, Vote}

  describe "boards" do
    test "can get existing boards" do
      board = insert(:board)

      assert Retrospective.list_boards() == [board]
    end

    test "can get boards by id" do
      board = insert(:board)

      assert Retrospective.get_board_by_id(board.id) == board
    end

    test "can create a board" do
      assert {:ok, %Board{title: "Test Title"}} =
               Retrospective.create_board(%{title: "Test Title"})
    end

    test "cannot create a board without a title" do
      assert {:error, %Ecto.Changeset{valid?: false}} = Retrospective.create_board(%{title: nil})
    end

    test "can update a board" do
      board = insert(:board, title: "Test Title")

      assert {:ok, %Board{title: "New Title"}} =
               Retrospective.update_board(board, %{title: "New Title"})
    end

    test "cannot update a board without a title" do
      board = insert(:board, title: "Test Title")

      assert {:error, %Ecto.Changeset{valid?: false}} =
               Retrospective.update_board(board, %{title: nil})
    end

    test "can delete a board" do
      board = insert(:board)

      assert {:ok, board} = Retrospective.delete_board(board)
      assert_raise Ecto.NoResultsError, fn -> Retrospective.get_board_by_id(board.id) end
    end
  end

  describe "columns" do
    test "can get all columns for a board" do
      board = insert(:board)
      insert(:column, board: board, title: "Test Column")

      assert [%Column{title: "Test Column"}] = Retrospective.get_columns_in_board(board.id)
    end

    test "can create a column in a board" do
      board = insert(:board)

      assert {:ok, %Column{title: "Test Column"}} =
               Retrospective.create_column(%{
                 board_id: board.id,
                 title: "Test Column"
               })
    end

    test "can edit a column in a board" do
      column = insert(:column, title: "Test Title")

      assert {:ok, %Column{title: "New Title"}} =
               Retrospective.update_column(column, %{title: "New Title"})
    end

    test "can delete a column in a board" do
      column = insert(:column)

      assert {:ok, column} = Retrospective.delete_column(column)
      assert [] = Retrospective.get_columns_in_board(column.board_id)
    end

    test "getting all columns is sorted by order key" do
      board = insert(:board)
      insert(:column, board: board, title: "First")
      insert(:column, board: board, title: "Second")
      insert(:column, board: board, title: "Third")
      insert(:column, board: board, title: "Fourth")

      assert [
               %Column{title: "First"},
               %Column{title: "Second"},
               %Column{title: "Third"},
               %Column{title: "Fourth"}
             ] = Retrospective.get_columns_in_board(board.id)
    end

    test "cannot create a column without a title" do
      board = insert(:board)

      assert {:error, %Ecto.Changeset{valid?: false}} =
               Retrospective.create_column(%{
                 title: nil,
                 board_id: board.id
               })
    end

    test "can move a column to first" do
      board = insert(:board)
      [before_column, _, _, _, _] = insert_list(5, :column, board: board)

      column = insert(:column, board: board, title: "New First")

      Retrospective.move_column_to_new_position(column, nil, before_column.rank)

      assert [
               %Column{title: "New First"},
               %Column{},
               %Column{},
               %Column{},
               %Column{},
               %Column{}
             ] = Retrospective.get_columns_in_board(board.id)
    end

    test "can move a column in the middle" do
      board = insert(:board)
      [_, _, after_column, before_column, _] = insert_list(5, :column, board: board)
      column = insert(:column, board: board, title: "Test Column")

      Retrospective.move_column_to_new_position(column, after_column.rank, before_column.rank)

      assert [
               %Column{},
               %Column{},
               %Column{},
               %Column{title: "Test Column"},
               %Column{},
               %Column{}
             ] = Retrospective.get_columns_in_board(board.id)
    end

    test "can move a column to the end" do
      board = insert(:board)

      [_, %Column{title: title} = column, _, after_column, _] =
        insert_list(5, :column, board: board)

      Retrospective.move_column_to_new_position(column, after_column.rank, nil)

      assert [
               %Column{},
               %Column{},
               %Column{},
               %Column{},
               %Column{title: ^title}
             ] = Retrospective.get_columns_in_board(board.id)
    end
  end

  describe "cards" do
    test "can get all cards in a column" do
      card = insert(:card, body: "Test Card")

      assert %Card{body: "Test Card"} = Retrospective.get_cards_in_column(card.column_id) |> hd()
    end

    test "can create a card" do
      column = insert(:column)

      assert {:ok, %Card{body: "Test Card"}} =
               Retrospective.create_card(%{column_id: column.id, body: "Test Card"})
    end

    test "can update a card" do
      card = insert(:card, body: "Test Card")

      assert {:ok, %Card{body: "Updated Card"}} =
               Retrospective.update_card(card, %{body: "Updated Card"})
    end

    test "can delete a card" do
      card = insert(:card, body: "Test Card")

      assert {:ok, %Card{body: "Test Card"}} = Retrospective.delete_card(card)
      assert [] = Retrospective.get_cards_in_column(card.column_id)
    end

    test "can get number of votes on a card" do
      card = insert(:card)
      insert_list(4, :vote, card: card)

      assert 4 = Retrospective.get_card_vote_count(card.id)
    end
  end

  describe "votes" do
    test "can vote on a card" do
      card = insert(:card)

      assert {:ok, %Vote{}} = Retrospective.vote_on_card(card.id)
    end

    test "can remove a vote on a card" do
      %Vote{id: vote_id} = vote = insert(:vote)

      assert {:ok, %Vote{id: ^vote_id}} = Retrospective.remove_vote_from_card(vote)
    end
  end
end
