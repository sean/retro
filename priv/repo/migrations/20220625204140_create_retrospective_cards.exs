defmodule Retro.Repo.Migrations.CreateRetrospectiveCards do
  use Ecto.Migration

  def change do
    create table(:cards) do
      add :body, :string, null: false
      add :column_id, references(:columns), null: false

      timestamps()
    end
  end
end
