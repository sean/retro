defmodule Retro.Repo.Migrations.RankWithLexorank do
  use Ecto.Migration

  def change do
    alter table(:columns) do
      add :rank, :string, default: LexorankEx.maximum_value(8)
      remove :order
    end

    create unique_index(:columns, [:board_id, :rank])

    alter table(:cards) do
      add :rank, :string, default: LexorankEx.maximum_value(8)
      remove :order
    end

    create unique_index(:cards, [:column_id, :rank])
  end
end
