defmodule Retro.Repo.Migrations.CreateRetrospectiveVotes do
  use Ecto.Migration

  def change do
    create table(:votes) do
      add :card_id, references(:cards), null: false

      timestamps()
    end
  end
end
