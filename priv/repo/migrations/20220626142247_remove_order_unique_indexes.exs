defmodule Retro.Repo.Migrations.RemoveOrderUniqueIndexes do
  use Ecto.Migration

  def change do
    drop unique_index(:columns, [:board_id, :order])
    drop unique_index(:cards, [:column_id, :order])
  end
end
