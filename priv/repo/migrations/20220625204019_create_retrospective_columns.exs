defmodule Retro.Repo.Migrations.CreateRetrospectiveColumns do
  use Ecto.Migration

  def change do
    create table(:columns) do
      add :title, :string, null: false
      add :order, :integer, null: false
      add :board_id, references(:boards), null: false

      timestamps()
    end
  end
end
