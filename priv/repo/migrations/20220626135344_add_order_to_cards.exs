defmodule Retro.Repo.Migrations.AddOrderToCards do
  use Ecto.Migration

  def change do
    alter table(:cards) do
      add :order, :integer, default: 0
    end

    create unique_index(:cards, [:column_id, :order])
  end
end
