defmodule Retro.Repo.Migrations.MakeColumnOrderUnique do
  use Ecto.Migration

  def change do
    create unique_index(:columns, [:board_id, :order])
  end
end
