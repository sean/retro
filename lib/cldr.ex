defmodule RetroCldr do
  use Cldr,
    default_locale: "en",
    locales: ["en"],
    otp_app: :retro,
    providers: [Cldr.Message],
    force_locale_download: false
end
