defmodule Retro.Retrospective do
  import Ecto.Query
  alias Retro.Repo
  alias Retro.Retrospective.{Board, Column, Card, Vote}

  def list_boards() do
    Repo.all(Board)
  end

  def get_board_by_id(id) do
    Repo.get!(Board, id)
  end

  def create_board(params) do
    %Board{}
    |> Board.changeset(params)
    |> Repo.insert()
  end

  def update_board(board, params) do
    board
    |> Board.changeset(params)
    |> Repo.update()
  end

  def delete_board(board) do
    Repo.delete(board)
  end

  def get_columns_in_board(board_id) do
    query =
      from c in Column,
        where: [board_id: ^board_id],
        order_by: [asc: c.rank]

    Repo.all(query)
  end

  def create_column(params) do
    %Column{}
    |> Column.changeset(params)
    |> Repo.insert()
  end

  def update_column(column, params) do
    column
    |> Column.changeset(params)
    |> Repo.update()
  end

  def delete_column(column) do
    Repo.delete(column)
  end

  def move_column_to_new_position(column, after_rank, nil) do
    column
    |> Column.changeset(%{rank: LexorankEx.next(after_rank)})
    |> Repo.update()
  end

  def move_column_to_new_position(column, nil, before_rank) do
    column
    |> Column.changeset(%{rank: LexorankEx.prev(before_rank)})
    |> Repo.update()
  end

  def move_column_to_new_position(column, after_rank, before_rank) do
    column
    |> Column.changeset(%{rank: LexorankEx.between(after_rank, before_rank)})
    |> Repo.update()
  end

  def get_cards_in_column(column_id) do
    query = from Card, where: [column_id: ^column_id]

    Repo.all(query)
  end

  def create_card(params) do
    %Card{}
    |> Card.changeset(params)
    |> Repo.insert()
  end

  def update_card(card, params) do
    card
    |> Card.changeset(params)
    |> Repo.update()
  end

  def delete_card(card) do
    Repo.delete(card)
  end

  def get_card_vote_count(card_id) do
    from(v in Vote, where: v.card_id == ^card_id) |> Repo.aggregate(:count, :id)
  end

  def vote_on_card(card_id) do
    %Vote{}
    |> Vote.changeset(%{card_id: card_id})
    |> Repo.insert()
  end

  def remove_vote_from_card(vote) do
    vote
    |> Repo.delete()
  end
end
