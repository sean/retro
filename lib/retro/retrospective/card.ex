defmodule Retro.Retrospective.Card do
  use Retro, :schema
  import Ecto.Changeset
  alias Retro.Retrospective.{Column, Vote}

  schema "cards" do
    field :body, :string
    field :rank, :string, default: LexorankEx.maximum_value(8)
    belongs_to :column, Column
    has_many :votes, Vote

    timestamps()
  end

  def changeset(model, params) do
    model
    |> cast(params, [:column_id, :body, :rank])
    |> validate_required([:column_id, :body, :rank])
  end
end
