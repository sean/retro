defmodule Retro.Retrospective.Column do
  use Retro, :schema
  import Ecto.Changeset
  alias Retro.Retrospective.{Board, Card}

  schema "columns" do
    field :title, :string
    field :rank, :string, default: LexorankEx.maximum_value(8)
    belongs_to :board, Board
    has_many :cards, Card

    timestamps()
  end

  def changeset(model, params) do
    model
    |> cast(params, [:title, :rank, :board_id])
    |> validate_required([:title, :rank, :board_id])
  end
end
