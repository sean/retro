defmodule Retro.Retrospective.Vote do
  use Retro, :schema
  import Ecto.Changeset
  alias Retro.Retrospective.Card

  schema "votes" do
    belongs_to :card, Card

    timestamps()
  end

  def changeset(model, params) do
    model
    |> cast(params, [:card_id])
    |> validate_required([:card_id])
  end
end
