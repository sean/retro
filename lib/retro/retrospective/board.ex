defmodule Retro.Retrospective.Board do
  use Retro, :schema
  import Ecto.Changeset
  alias Retro.Retrospective.{Column, Card}

  schema "boards" do
    field :title, :string
    has_many :columns, Column
    has_many :cards, through: [:columns, Card]

    timestamps()
  end

  def changeset(model, params) do
    model
    |> cast(params, [:title])
    |> validate_required([:title])
  end
end
