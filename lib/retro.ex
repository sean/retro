defmodule Retro do
  @moduledoc """
  Retro keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  @doc """
  Helpers for easily creating ecto schemas
  """
  def schema do
    quote location: :keep do
      use Ecto.Schema
    end
  end

  @doc """
  When used, apply the appropriate helper macro
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
